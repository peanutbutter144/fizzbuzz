#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/*
 * fizzbuzz - play the fizzbuzz game
 * Copyright (C) 2018  Tom Ient <tomient@posteo.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

int main()
{
	char message[8]; // message string - longest it should be is 8 ("FizzBuzz")
	for (int i = 1; i <= 100; i++) {
		message[0] = '\0'; // reset to empty string

		if (i % 3 == 0) {
			strcat(message, "Fizz"); // append "Fizz" to message
		}

		if (i % 5 == 0) {
			strcat(message, "Buzz");
		}

		if ((i % 3 != 0) && (i % 5 != 0)) {
			sprintf(message, "%d", i); // convert int to string - not printing anything (despite the name)
		}

		printf("%s\n", message);
	}

	return 0;
}
